'''
Import PyData and Plotly Libraries
'''
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from plotly.offline import plot, iplot, download_plotlyjs
import cufflinks as cf

'''
Activate offline mode for Cufflinks
'''
cf.go_offline()

'''
Load and clean up data to only analyze 2015
and the sale of conventional avocados
'''
avo_data = pd.read_csv('avocado.csv')
avo_data = avo_data[avo_data['type']=='organic']

'''
Seaborn Factor Plot Creation to see
all Regions Price Range in the same
chart
'''
sns.set_style('white')
order = avo_data[avo_data['year']==2018].groupby('region')['AveragePrice'].mean().sort_values().index
all_regions = sns.catplot(kind='point',
                            x='AveragePrice',
                            y='region',
                            hue='year',
                            data=avo_data,
                            palette='Blues',
                            height=8,
                            aspect=0.6,
                            order=order,
                            join=False)
all_regions.savefig('All Regions Factor Plot (Organic)')