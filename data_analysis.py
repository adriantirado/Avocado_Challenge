'''
Adrian Tirado 
ECSE 2021
Rensselaer Polytechnic Institute

This is a simple script for the exploratory analysis of 
Avocado Sales Data using PyData libraries to analyze the data
and Plotly to create interactive plots
'''

'''
Import PyData and Plotly Libraries
'''
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from plotly.offline import plot, iplot, download_plotlyjs
import cufflinks as cf

'''
Activate offline mode for Cufflinks
'''
cf.go_offline()

'''
Load and clean up data to only analyze 2015
and the sale of conventional avocados
'''
avo_data = pd.read_csv('avocado.csv')
avo_data = avo_data[avo_data['type'] == 'conventional']
avo_data = avo_data[avo_data['year'] == 2015]

'''
Get all the unique regions in the data set
'''
regions = avo_data['region'].unique()

'''
Create an interactive plotly chart for sales of PLU 4046 and 4225 
within each region in the data set

Also add a scatterplot of the average price on that same date
'''
for region in regions:
    
    '''
    Create Regional Data Set
    '''
    regional_data = avo_data[avo_data['region'] == region]
    regional_data.set_index('Date', inplace=True)

    '''
    Create the two figures:
        Figure1 represents the bar plot portion of the chart
        Figure2 represents the line plot portion of the chart
    '''
    fig1 = regional_data[['4046', '4225']].iplot(kind='bar', asFigure=True)
    fig2 = regional_data['AveragePrice'].iplot(kind='scatter',barmode='stack', xTitle='Dates',
                                      title=('Avocado Sales and Prices('+region+')'),colors=['purple'], secondary_y='AveragePrice', secondary_y_title='Price',asFigure=True)
    
    '''
    Merge both figures
    '''
    fig2['data'].extend(fig1['data'])

    '''
    Due to a bug on cufflinks I had to manually add
    the primary axis label manually.
    '''
    fig2['layout']['yaxis1'] = dict(
            gridcolor='#E1E5ED',
            showgrid=True,
            tickfont=dict(
                color='#4D5663'
            ),
            title='Volume',
            titlefont=dict(
                color='#4D5663'
            ),
            zerolinecolor='#E1E5ED'
        )
    '''
    Plot both figures into the html file
    '''
    plot(fig2, auto_open=False, filename=('Avocado_Sales_Prices(ALL_REGIONS)/Avocado_Sales_'+region+'.html'))

'''
Seaborn Factor Plot Creation to see
all Regions Price Range in the same
chart
'''
sns.set_style('white')
order = avo_data.groupby('region')['AveragePrice'].mean().sort_values().index
all_regions = sns.catplot(kind='point',
                            x='AveragePrice',
                            y='region',
                            data=avo_data,
                            height=8,
                            aspect=0.6,
                            order=order,
                            join=False)
all_regions.savefig('All Regions Factor Plot(Conventional)')
